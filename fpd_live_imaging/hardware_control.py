import time


class DetectorMotor(object):

    def __init__(self, serial_port='COM1', baud_rate=115200):
        self.serial_port = serial_port
        self.baud_rate = baud_rate
        try:
            import serial
        except ImportError:
            raise Exception('pyserial module cannot be loaded.')

    def insert(self):
        try:
            import serial
            ser = serial.Serial(self.serial_port, self.baud_rate)
            time.sleep(0.5)
            ser.write(b"merin = 1\r")
            time.sleep(0.5)
            ser.close()
        except serial.serialutil.SerialException:
            print("Can not connect to serial port " + self.serial_port)

    def retract(self):
        try:
            import serial
            ser = serial.Serial(self.serial_port, self.baud_rate)
            time.sleep(0.5)
            ser.write(b"merin = 0\r")
            time.sleep(0.5)
            ser.close()
        except serial.serialutil.SerialException:
            print("Can not connect to serial port " + self.serial_port)
