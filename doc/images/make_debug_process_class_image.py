import os
import numpy as np
import multiprocessing as mp
import fpd_live_imaging.process_classes as pc
import fpd_live_imaging.testing_tools as tt
import matplotlib.pyplot as plt

my_path = os.path.join(os.path.dirname(__file__), 'debugging')
if not os.path.exists(my_path):
    os.makedirs(my_path)

output_queue = mp.Queue()
full_diff = pc.FullDiffractionImageFFTProcess('test_full_fft', output_queue)
receive_queue = full_diff.receive_queue

# Sending data to process
test_detector = tt.TestDetectorPeriodicStructure()
input_data = test_detector._get_detector_data()
receive_queue.put(input_data)
full_diff.start_process_function()

# Getting data after process
output_data = output_queue.get()
full_diff.stop_running()

fig, axarr = plt.subplots(1, 2, figsize=(10, 5))
axarr[0].imshow(input_data)
axarr[1].imshow(output_data)
fig.savefig(os.path.join(my_path, "compare_process_class.jpg"))
