Fast pixelated detector live imaging
====================================

Library for live processing and visualization of data acquired on a fast pixelated detector in a scanning transmission electron microscope.

The source code is accessible at https://gitlab.com/fast_pixelated_detectors/fpd_live_imaging.

Contributions, feature requests and bug reports are welcome on the `issues page <https://gitlab.com/fast_pixelated_detectors/fpd_live_imaging/issues>`_.

.. figure:: images/live_imaging_center_of_mass.jpg
    :scale: 50 %
    :align: center

    Live imaging of magnetic structures using thresholded center of mass

.. toctree::
   install
   tutorial
   debugging
   api_documentation
   related_projects
   :maxdepth: 2
   :caption: Contents:

Contributions from Magnus Nord funded by EPSRC via the project "Fast Pixel Detectors: a paradigm shift in STEM imaging" (Grant reference EP/M009963/1).

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
