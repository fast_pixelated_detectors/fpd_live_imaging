.. _install:

==========
Installing
==========

Anaconda
########

The easiest way is to install using the Anaconda Python environment `Anaconda environment <https://www.continuum.io/downloads>`_.

Windows
-------

After installing the Python 3 version of Anaconda, open the `Anaconda prompt` (Start menu - Anaconda3) and run:

.. code-block:: bash

    $ pip install fpd_live_imaging


Continue with the :ref:`tutorial`.


Linux
-----

Open the Anaconda prompt and run:

.. code-block:: bash

    $ pip install fpd_live_imaging


Next step: :ref:`tutorial`.

Linux, with system packages
###########################

In Linux the library can be installed from PyPI directly:

.. code-block:: bash

    $ pip install fpd_live_imaging


Continue with the :ref:`tutorial`.
