.. _debugging:

=========
Debugging
=========

Due to the many different processing steps in ``fpd_live_imaging`` it can sometimes be hard to debug various functionalties.
For example, if a new ``process_class`` doesn't show anything in the live view window, it can be either due to the process class itself, or some bug in the live view class.
This page shows how to debug individual parts of ``fpd_live_imaging``.


Process classes
===============

.. code-block:: python

   >>> import multiprocessing as mp
   >>> import fpd_live_imaging.process_classes as pc
   >>> import fpd_live_imaging.testing_tools as tt
   >>> output_queue = mp.Queue()
   >>> full_diff = pc.FullDiffractionImageFFTProcess('test_full_fft', output_queue)
   >>> receive_queue = full_diff.receive_queue

   >>> # Sending data to process
   >>> test_detector = tt.TestDetectorPeriodicStructure()
   >>> input_data = test_detector._get_detector_data()
   >>> receive_queue.put(input_data)
   >>> full_diff.start_process_function()

   >>> # Getting data after process
   >>> output_data = output_queue.get()
   >>> full_diff.stop_running()


Plotting the data

.. code-block:: python

   >>> import matplotlib.pyplot as plt
   >>> fig, axarr = plt.subplots(1, 2, figsize=(10, 5))
   >>> cax = axarr[0].imshow(input_data)
   >>> cax = axarr[1].imshow(output_data)
   >>> fig.savefig("compare_process_class.jpg")


.. figure:: images/debugging/compare_process_class.jpg
    :scale: 50 %
    :align: center


Live imaging parallel
=====================

.. code-block:: python

   >>> import time
   >>> import multiprocessing as mp
   >>> import fpd_live_imaging.process_classes as pc
   >>> import fpd_live_imaging.testing_tools as tt
   >>> import fpd_live_imaging.live_qt_visualization as lqv

   >>> # Setting up the live imaging
   >>> annotation_array = mp.Array('i', 4)
   >>> live_parallel = lqv.LiveParallelImageQt(annotation_array=annotation_array, name='fulldiff')

   >>> # Starting the process class
   >>> output_queue = live_parallel.input_queue
   >>> full_fft_diff = pc.FullDiffractionImageFFTProcess('test_full_fft', output_queue)
   >>> receive_queue = full_fft_diff.receive_queue

   >>> # Starting live parallel
   >>> test_detector = tt.TestDetectorPeriodicStructure()
   >>> full_fft_diff.start_process_function()
   >>> live_parallel.start_data_listening()

   >>> # Sending data to process
   >>> for i in range(10):
   ...     receive_queue.put(test_detector._get_detector_data())
   ...     time.sleep(1)

   >>> # Stopping the processes
   >>> full_fft_diff.stop_running()
   >>> live_parallel.stop_running()
