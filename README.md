### Webpage for the current release: [https://fast_pixelated_detectors.gitlab.io/fpd_live_imaging/](https://fast_pixelated_detectors.gitlab.io/fpd_live_imaging/)
### Webpage for the development branch: [https://gitlab.com/fast_pixelated_detectors/fpd_live_imaging/builds/artifacts/master/file/pages_development/index.html?job=pages_development](https://gitlab.com/fast_pixelated_detectors/fpd_live_imaging/builds/artifacts/master/file/pages_development/index.html?job=pages_development)

## Download of Anaconda package

#### Current release (recommended)

These packages (should) be fairly stable.

[Linux 64-bit](https://gitlab.com/fast_pixelated_detectors/fpd_live_imaging/builds/artifacts/release/raw/fpd_live_imaging-linux64.tar.bz2?job=anaconda_install_tests)

[Windows 64-bit](https://gitlab.com/fast_pixelated_detectors/fpd_live_imaging/builds/artifacts/release/raw/fpd_live_imaging-win64.tar.bz2?job=anaconda_install_tests)


#### Development version

These packages are made from the current master branch, so they might not work properly.

[Linux 64-bit](https://gitlab.com/fast_pixelated_detectors/fpd_live_imaging/builds/artifacts/master/raw/fpd_live_imaging-linux64.tar.bz2?job=anaconda_install_tests)

[Windows 64-bit](https://gitlab.com/fast_pixelated_detectors/fpd_live_imaging/builds/artifacts/master/raw/fpd_live_imaging-win64.tar.bz2?job=anaconda_install_tests)


## Installing

Anaconda (Python 3.6 version) needs to be install beforehand: https://www.continuum.io/downloads

#### Windows

Start the Anaconda Prompt (Win7: Anaconda3/Anaconda Prompt). In this terminal, write:

```bash
conda install fpd_live_imaging-linux64.tar.bz2
```

## Using

Start the IPython terminal (Win7: Anaconda3/IPython).

In the IPython terminal:

```python
import fpd_live_imaging.testing_tools as tt
import fpd_live_imaging.acquisition_control_class as acc
from fpd_live_imaging.test_images.test_images import linux_penguin_64

acquisition_control = acc.LiveImagingQt()
test_detector = tt.TestDetectorInputImage(number_of_frames=64*64, input_image=linux_penguin_64)
test_detector.sleep_time.value = 0.001
test_detector.start_data_listening()
acquisition_control._comm_medi.port = test_detector.port
acquisition_control.start_bf_process()
acquisition_control.resize_scan(64, 64)
acquisition_control.start_medipix_receiver()
```

Note: running the above code might not work on Windows, especially if the code is copied
into a file to run it as a script. The code below should work for this:

```python
import fpd_live_imaging.testing_tools as tt
import fpd_live_imaging.acquisition_control_class as acc
from fpd_live_imaging.test_images.test_images import linux_penguin_64

if '__name__' == '__main__':
    acquisition_control = acc.LiveImagingQt()
    test_detector = tt.TestDetectorInputImage(number_of_frames=64*64, input_image=linux_penguin_64)
    test_detector.sleep_time.value = 0.001
    test_detector.start_data_listening()
    acquisition_control._comm_medi.port = test_detector.port
    acquisition_control.start_bf_process()
    acquisition_control.resize_scan(64, 64)
    acquisition_control.start_medipix_receiver()
```
